package main

import (
	pb "gb-cinema-payment/api"
	"google.golang.org/grpc"
)

var UserStorageClient pb.UserStorageClient

func NewUserStorageClient(addr string) (pb.UserStorageClient, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}
	return pb.NewUserStorageClient(conn), nil
}
