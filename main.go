package main

import (
	"context"
	pb "gb-cinema-payment/api"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

var cfg = struct {
	Port     int
	UserAddr string
	WebAddr  string
}{
	Port:     8083,
	WebAddr:  "http://localhost:8080",
	UserAddr: "localhost:8082",
}

func main() {
	var err error

	log.Println("Connecting to gRPC user service ...")
	UserStorageClient, err = NewUserStorageClient(cfg.UserAddr)
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/checkout", checkoutFormHandler).Methods("GET")
	r.HandleFunc("/checkout", checkoutHandler).Methods("POST")

	SetTemplateDir(".")
	AddTemplate("payform", "payform.html")
	AddTemplate("msg", "msg.html")
	err = ParseTemplates()
	if err != nil {
		log.Fatalf("Init template error: %v", err)
	}

	log.Printf("Starting on port %d", cfg.Port)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(cfg.Port), r))
}

func checkoutFormHandler(w http.ResponseWriter, r *http.Request) {
	uid := r.FormValue("uid")
	if uid == "" {
		RenderTemplate(w, "msg", Msg{
			"Не указан идентификатор пользователя",
			cfg.WebAddr,
		})
		return
	}

	RenderTemplate(w, "payform", struct{ Uid string }{uid})
}

type Msg struct {
	Msg     string
	BackURL string
}

func checkoutHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	uid := r.FormValue("uid")
	userID, err := strconv.Atoi(uid)
	if err != nil {
		RenderTemplate(w, "msg", Msg{
			"Не валидный uid",
			"/checkout?uid=" + uid,
		})
	}

	if !makePayment(
		r.FormValue("pan"),
		r.FormValue("date"),
		r.FormValue("cvc"),
	) {
		RenderTemplate(w, "msg", Msg{
			"Не верные платежные данные",
			"/checkout?uid=" + uid,
		})
	}

	/*
		err := PatchJSON(
			cfg.UserAddr+"/user",
			url.Values{
				"id":      []string{uid},
				"is_paid": []string{"true"},
			},
			nil,
		)
	*/

	req := &pb.UserIdAndIspaid{
		UserId: uint64(userID),
		IsPaid: true,
	}
	log.Println("user id =", userID)
	resp, err := UserStorageClient.PatchUserPaid(context.Background(), req)
	log.Println(resp)

	if err != nil {
		log.Printf("Payment error: %v", err)
		RenderTemplate(w, "msg", Msg{
			"Во время проведения платежа произошла ошибка",
			"/checkout?uid=" + uid,
		})
		return
	}

	RenderTemplate(w, "msg", Msg{
		"Платеж успешно совершен. Для применения настроек, пожалуйста, залогиньтесь на сайт заново.",
		cfg.WebAddr + "/logout",
	})
	return
}

func makePayment(pan, date, cvc string) bool {
	if pan != "4444444444444444" && date != "12/12" && cvc != "123" {
		return false
	}
	return true
}
